<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\TouchSpin;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'item_name')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'price_each')->textInput() ?>

	<?=$form->field($model, 'quantity_in_stock')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'הכנס כמות'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>
	
	
  

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'צור' : 'עדכן', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
