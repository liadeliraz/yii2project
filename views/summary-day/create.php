<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SummaryDay */

$this->title = 'הכנס סיכום יום';
$this->params['breadcrumbs'][] = ['label' => 'סיכום יום', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="summary-day-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
