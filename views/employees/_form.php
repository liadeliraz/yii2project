<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Employees;
use app\models\Role;
use app\models\Armed;
use app\models\PercentOfJobs;
use app\models\User;


/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employees-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->input('email') ?>
	
	<?= $form->field($model, 'role')->dropDownList(Role::getRoles(),['prompt' => 'בחר תפקיד']) ?>   
	
	

    
	<?= $form->field($model, 'Percent_of_jobs')->dropDownList(PercentOfJobs::getPercentOfJobss(),['prompt' => 'בחר אחוז משרה']) ?>
    
	<?= $form->field($model, 'armed')->dropDownList(Armed::getArmeds(),['prompt' => 'בחר סוג נשק']) ?> 

	
   <?= $form->field($model, 'cellphone')->textInput() ?>

    <?= $form->field($model, 'adress')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'file')->fileInput()->label('תמונה') ?> 

   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'צור עובד' : 'עדכון', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
