<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'עובדים';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('צור עובד חדש', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('המרה לאקסל', ['export'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'last_name',

			
			[
				'attribute' => 'role',
				'label' => 'תפקיד',
				'format' => 'raw',
				'value' => function($model){
					return $model->roleItem->name;
				},
				'filter'=>Html::dropDownList('EmployeesSearch[role]', $role, $roles, ['class'=>'form-control']),
			],			
			
			
				//'Percent_of_jobs',
           [
				'attribute' => 'Percent_of_jobs',
				'label' => 'אחוז משרה',
				'format' => 'raw',
				'value' => function($model){
					return $model->percent_of_jobsItem->name;
				},
				'filter'=>Html::dropDownList('EmployeesSearch[Percent_of_jobs]', $Percent_of_jobs, $PercentOfJobss, ['class'=>'form-control']),
			],		
		   
		   
           // 'armed',
			[
				'attribute' => 'armed',
				'label' => 'נשק',
				'format' => 'raw',
				'value' => function($model){
					return $model->armedItem->name;
				},
				'filter'=>Html::dropDownList('EmployeesSearch[armed]', $armed, $armeds, ['class'=>'form-control']),
			],			
 'cellphone',
            'adress',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
