<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'עובדים', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('עדכון', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
	
	
	
<!--<img style= "weight:100px; height:100px;" src="web/uploads/רם.jpg "></img>-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
           // 'role',
			
			[ // the role name 
				'label' => $model->attributeLabels()['role'],
				'value' => $model->roleItem->name,	
			],			
			
          
			 //'Percent_of_jobs',
			[ // the Percent_of_jobs name 
				'label' => $model->attributeLabels()['Percent_of_jobs'],
				'value' => $model->percent_of_jobsItem->name,	
			],	
			[ // the armed name 
				'label' => $model->attributeLabels()['armed'],
				'value' => $model->armedItem->name,	
			],	
			'email',
            'cellphone',
            'adress',
           
			

			[
				'label' => 'נתיב התמונה' ,
				'value' => $model->image,
			
			],
        ],
    ]) ?>
		
</div>
