<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Visitors */

$this->title = 'עדכון נתונים ' . $model->date;
$this->params['breadcrumbs'][] = ['label' => 'מבקרים', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->date, 'url' => ['view', 'id' => $model->date]];
$this->params['breadcrumbs'][] = 'עדכון נתונים';
?>
<div class="visitors-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
