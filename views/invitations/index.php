<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Suppliers;
use app\models\Item;
use dosamigos\datepicker\DatePicker;


/* @var $this yii\web\View */
/* @var $searchModel app\models\InvitationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'הזמנות';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invitations-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('צור הזמנה', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('המרה לאקסל', ['export'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

			
           
			[
				'attribute' => 'item_name',
				'label' => 'שם פריט',
				'format' => 'raw',
				'value' => function($model){
					return $model->item->item_name;
				},
				'filter'=>Html::dropDownList('InvitationsSearch[item_name]', $item_name, $item_names, ['class'=>'form-control']),
			],
            [
				'attribute' => 'supplier_name',
				'label' => 'שם ספק',
				'format' => 'raw',
				'value' => function($model){
					return $model->suplirsname->supplier_name;
				},
				'filter'=>Html::dropDownList('InvitationsSearch[supplier_name]', $supplier_name, $supplier_nameS, ['class'=>'form-control']),
			],

            'open_date',
			[
				'attribute' => 'due_date',
				'value' => 'due_date',
				'format' => 'raw',
				'filter' => DatePicker::widget([
						'model' => $searchModel,
						'attribute' => 'due_date',
						'clientOptions' => [
						'autoclose' => true,
						'format' => 'dd/mm/yyyy']
							
						])
			],
            //'due_date',
            'quantity_order',
             'approval_status',
             'order_status',
            // 'notes',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
