<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Invitations */

$this->title = 'עידכון הזמנה: ' . $model->item_name;
$this->params['breadcrumbs'][] = ['label' => 'הזמנות', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->item_name, 'url' => ['view', 'item_name' => $model->item_name, 'supplier_name' => $model->supplier_name]];
$this->params['breadcrumbs'][] = 'עידכון';
?>
<div class="invitations-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
