<?php

namespace app\models;

use Yii;
use miloschuman\highcharts\Highcharts;
use yii\filters\AccessControle;
/**
 * This is the model class for table "summaryDay".
 *
 * @property string $date
 * @property integer $israels
 * @property integer $tourist
 * @property integer $matmon
 * @property string $events
 * @property string $notes
 */
class SummaryDay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'summaryDay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'israels', 'matmon'], 'required','message'=>'שדה חובה'],
            [['date'], 'safe'],
            [['israels', 'tourist', 'matmon'], 'integer','message'=>'ספרות בלבד'],
            [['events', 'notes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => 'תאריך',
            'israels' => 'כמות ישראלים',
            'tourist' => 'כמות תיירים',
            'matmon' => 'כמות מנויים',
            'events' => 'ארועים ואכיפות',
            'notes' => 'הערות',
        ];
    }
}
