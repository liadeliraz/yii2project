<?php

namespace app\models;

use Yii;

use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "suppliers".
 *
 * @property string $supplier_name
 * @property string $phoneNum
 * @property string $address
 * @property string $contact
 * @property string $contactPhone
 * @property string $category
 * @property string $notes
 */
class Suppliers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suppliers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['supplier_name', 'phoneNum','category'], 'required','message'=>'שדה חובה'],
            [['phoneNum','supplier_name'], 'string', 'max' => 100],
            [['address',  'contact', 'category', 'notes','contactPhone'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'supplier_name' => 'שם ספק',
            'phoneNum' => 'טלפון',
            'address' => 'כתובת',
            'contact' => 'שם איש קשר',
            'contactPhone' => 'טלפון איש קשר',
            'category' => 'קטגוריה',
            'notes' => 'הערות',
        ];
    }
	

	public static function getSupplierss()
	{
		$allSupplierss = self::find()->all();
		$allSupplierssArray = ArrayHelper::
					map($allSupplierss, 'supplier_name' ,'supplier_name');
		return $allSupplierssArray;						
	}
	
	public static function getSupplierssWithAllSupplierss()
	{
		$allSupplierss = self::getSupplierss();
		$allSupplierss[-1] = 'כל הספקים';
		$allSupplierss = array_reverse ( $allSupplierss, true );
		return $allSupplierss;	
	}	
	
  public function getInvitations()
    {
        return $this->hasOne(Invitations::className(), ['supplier_name' => 'phoneNum']);
    }
	
	
	
	
	
}

