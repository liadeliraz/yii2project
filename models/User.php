<?php

	namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;	
	
	class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
	{

	public $role; //תכונה ידנית לטבלת יוזר כדי שיוזר מסוים יקבל תפקיד/הרשאה
	
    
    public static function tableName()
    {
        return 'user';
    }

	public function rules()
    {
        return  
		[
            [['username', 'password', 'auth_key', ], 'string', 'max' => 255],				
			[['username', 'id', 'password', ], 'required','message'=>'שדה חובה'],
			['role', 'safe'], //חוק לתכונה תפקיד/הרשאה
			[['created_at', 'updated_at', ], 'integer'],
			[['created_by', 'updated_by', ], 'integer'],
            [['username'], 'unique','message'=>'שם משתמש תפוס'],			
        ];				
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ת.ז',
            'username' => 'שם משתמש',
            'password' => 'סיסמא',
			'role'  => 'תפקיד המשתמש',
            'auth_key' => 'Auth Key',
			
        ];
    }	
	

	public function behaviors() //פונקציה שקשורה לארבע שדות תחקור
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

	

	public function getUserole() //קשור לתפקידים שבאים מטבלת אאוס אסיימנט
    {
		$roleArray = Yii::$app->authManager->getRolesByUser($this->id);
		$role = array_keys($roleArray)[0];				
		return	$role;
    }
	
	public function getEmployeessUser() //פונקציה שמקשרת את טבלת משתמשים לטבלת עובדים
    {
        return $this->hasMany(Employees::className(), ['id' => 'id']);
    }
	
	
	
	
	/*public static function getUsersWithAllUsers()
	{
		$users = self::getUsers();
		$users[-1] = 'All Users';
		$users = array_reverse ( $users, true );
		return $users;	
	}	*/

	public static function getRoles()   // פונקציה שקשורה לארביאיסי , פונקציה זו הולכת לטבלת אאוס אייטם ומביאה את התפקידים המוגדרים בה. במקרה שלנו תחזיר מערך עם אדמין, ראש צוות ועובד
	{
		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		return $roles; 	
	}

	/*public static function getRolesWithAllRoles()
	{
		$roles = self::getRoles();
		$roles[-1] = 'All Roles';
		$roles = array_reverse ( $roles, true );
		return $roles;	
	}	*/	

    public static function findIdentity($id) //פונקציה שקשורה לאימות משתמשים, הנדרשת ע"י האינטרפייס
    {
        return static::findOne($id);
    }

	public static function findByUsername($username) //פונקציה שקשורה לאימות משתמשים, הנדרשת ע"י האינטרפייס
	{
		return static::findOne(['username' => $username]);
	}

	
    public static function findIdentityByAccessToken($token, $type = null) //פונקציה שקשורה לאימות משתמשים, הנדרשת ע"י האינטרפייס
    {
		throw new NotSupportedException('You can only login
							by username/password pair for now.');
    }

 
    
    public function getId() //פונקציה שקשורה לאימות משתמשים, הנדרשת ע"י האינטרפייס
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() //פונקציה שקשורה לאימות משתמשים, הנדרשת ע"י האינטרפייס
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey) //פונקציה שקשורה לאימות משתמשים, הנדרשת ע"י האינטרפייס
    {
         return $this->getAuthKey() === $authKey;
    }	

	public function validatePassword($password) //פונקציה שקשורה לאימות משתמשים, הנדרשת ע"י האינטרפייס
	{
		return $this->isCorrectHash($password, $this->password); 
	}

	private function isCorrectHash($plaintext, $hash) //פונקציה שקשורה לאימות משתמשים, הנדרשת ע"י האינטרפייס
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}

    public function beforeSave($insert) //פונקציה שקשורה לאימות משתמשים, הנדרשת ע"י האינטרפייס פונקציה זו מופעלת לפני כל שמירה של רשומה
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	
	public function afterSave($insert,$changedAttributes) // פונקציה שמתבצעת אחרי שמירה של רשומת יוזר, היא מכניסה לטבלת אאוס אסיימנט יוזר בעל תפקיד מסוים על פי מה שהושם לאותו יוזר
    {
        $return = parent::afterSave($insert, $changedAttributes);

        
		if (!\Yii::$app->user->can('updateUser')){
			return $return;
		}

		$auth = Yii::$app->authManager;
		
		$roleName = $this->role; 
		$id = $this->id; 
		$role = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null) //בודקת האם משתמש קיבל השמה בעבר, עי הפונקציה שמביאה לי את התפקיד של היוזר בעל הת.ז הספציפית הזו, אם זה שווה  נאל זה אומר שלא קיבל השמה בעבר 
		{ 
			//$y = yii::$app->user->getId;
			//yii::$app->user->assign($y);
			$auth->assign($role, $id);			//עושה השמה ליוזר עם התפקיד שנבחר
			
		} 
		else { //זה אם אני רוצה לעדכן תתפקיד של היוזר
			$db = \Yii::$app->db; //אז אני מוחקת מהדאטא בייס את הרשומה המקורית ומכניסה את החדשה
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id); //פונקציה זו עושה השמה של התפקיד למשתמש
		}

        return $return;
    }	
	
}