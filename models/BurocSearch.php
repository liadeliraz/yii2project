<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Buroc;

/**
 * BurocSearch represents the model behind the search form about `app\models\Buroc`.
 */
class BurocSearch extends Buroc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'treatment', 'DueDate', 'creatDate', 'notes'], 'safe'],
            [['bstatus'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Buroc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		$this->bstatus == -1 ? $this->bstatus = null : $this->bstatus; 
		

        // grid filtering conditions
        $query->andFilterWhere([
            'bstatus' => $this->bstatus,
            'DueDate' => $this->DueDate,
            'creatDate' => $this->creatDate,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'treatment', $this->treatment])
            ->andFilterWhere(['like', 'notes', $this->notes]);

        return $dataProvider;
    }
}
