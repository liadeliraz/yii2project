<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "role".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Employees $employees
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	
	
	public static function getRoles()
	{
		$allRoles = self::find()->all();
		$allRolesArray = ArrayHelper::
					map($allRoles, 'id', 'name');
		return $allRolesArray;						
	}
	
	public static function getRolesWithAllRoles()
	{
		$allRoles = self::getRoles();
		$allRoles[-1] = 'כל התפקידים';
		$allRoles = array_reverse ( $allRoles, true );
		return $allRoles;	
	}		

	
	
	
	

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeesss()
    {
        return $this->hasOne(Employees::className(), ['role' => 'id']);
    }
}
