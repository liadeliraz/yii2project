<?php

use yii\db\Migration;

class m161019_093254_alter_employees_table extends Migration
{
    public function up()
    {
		$this->addColumn('employees','created_at','integer');
		$this->addColumn('employees','updated_at','integer');
		$this->addColumn('employees','created_by','integer');
		$this->addColumn('employees','updated_by','integer');
    }

    public function down()
    {
        $this->dropColumn('employees','created_at');
		$this->dropColumn('employees','updated_at');
		$this->dropColumn('employees','created_by');
		$this->dropColumn('employees','updated_by');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
