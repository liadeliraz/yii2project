<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$employee = $auth->createRole('employee');
		$auth->add($employee);
		
		$teamleader = $auth->createRole('teamleader');
		$auth->add($teamleader);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

	public function actionTmpermissions()
	{
		$auth = Yii::$app->authManager;
		
		
		$updateOwnUser = $auth->createPermission('updateOwnUser');
		$updateOwnUser->description = 'Every user can update his/her own profile ';
		$auth->add($updateOwnUser);
		
		$updateOwnPassword  = $auth->createPermission('updateOwnPassword');
		$updateOwnPassword->description = 'Every user can update his/her own password';
		$auth->add($updateOwnPassword);		
				
		$viewOwnUser = $auth->createPermission('viewOwnUser');
		$viewOwnUser->description = 'Every user can view his/her own profile ';
		$auth->add($viewOwnUser);
		
		$viewOwnEmployee = $auth->createPermission('viewOwnEmployee');
		$viewOwnEmployee->description = 'Every employee can view his/her own profile ';
		$auth->add($viewOwnEmployee);
		
		$indexEmployee = $auth->createPermission('indexEmployee');
		$indexEmployee->description = 'Index employee';
		$auth->add($indexEmployee);
		
		$viewSummary = $auth->createPermission('viewSummary');
		$viewSummary->description = 'View summary';
		$auth->add($viewSummary);

		$indexSummary = $auth->createPermission('indexSummary');
		$indexSummary->description = 'Index summary';
		$auth->add($indexSummary);

		$viewProject = $auth->createPermission('viewProject');
		$viewProject->description = 'View Project';
		$auth->add($viewProject);

		$indexProject = $auth->createPermission('indexProject');
		$indexProject->description = 'Index Project';
		$auth->add($indexProject);
	}


	public function actionTlpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createSummary = $auth->createPermission('createSummary');
		$createSummary->description = 'Team leader can create new summary';
		$auth->add($createSummary);
		
		$updateSummary = $auth->createPermission('updateSummary');
		$updateSummary->description = 'Team leader can update summary';
		$auth->add($updateSummary);		
		
		$createProject = $auth->createPermission('createProject');
		$createProject->description = 'Team leader can create project';
		$auth->add($createProject);		
		
		$updateProject = $auth->createPermission('updateProject');
		$updateProject->description = 'Team leader can update project';
		$auth->add($updateProject);		
		
		$updateOwnProject = $auth->createPermission('updateOwnProject');
		$updateOwnProject->description = 'Team leader can update his only project';
		$auth->add($updateOwnProject);
			
		$updateOwnSummary = $auth->createPermission('updateOwnSummary');
		$updateOwnSummary->description = 'Team leader can update his only summary';
		$auth->add($updateOwnSummary);	
		
	}
	

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createUser = $auth->createPermission('createUser');
		$createUser->description = 'Admin can create new user';
		$auth->add($createUser);
		
		$updateUser = $auth->createPermission('updateUser');
		$updateUser->description = 'Admin can update all users';
		$auth->add($updateUser);

		$deleteUser = $auth->createPermission('deleteUser');
		$deleteUser->description = 'Admin can delete users';
		$auth->add($deleteUser);

		$updatePassword = $auth->createPermission('updatePassword');
		$updatePassword->description = 'Admin can update password for all users';
		$auth->add($updatePassword);
		
		$indexUser = $auth->createPermission('indexUser');
		$indexUser->description = 'Admin can view all users';
		$auth->add($indexUser);
		
		$viewUser = $auth->createPermission('viewUser');
		$viewUser->description = 'Admin can view each user';
		$auth->add($viewUser);
		
		$createEmployee = $auth->createPermission('createEmployee');
		$createEmployee->description = 'Admin can create new employee';
		$auth->add($createEmployee);
		
		$updateEmployee = $auth->createPermission('updateEmployee');
		$updateEmployee->description = 'Admin can update  employee';
		$auth->add($updateEmployee);
		
		$deleteEmployee = $auth->createPermission('deleteEmployee');
		$deleteEmployee->description = 'Admin can delete  employee';
		$auth->add($deleteEmployee);
		
		$viewEmployee = $auth->createPermission('viewEmployee');
		$viewEmployee->description = 'Admin can view each  employee';
		$auth->add($viewEmployee);
		
		$createBuroc = $auth->createPermission('createBuroc');
		$createBuroc->description = 'Admin can create new buroc';
		$auth->add($createBuroc);
		
		$updateBuroc = $auth->createPermission('updateBuroc');
		$updateBuroc->description = 'Admin can update  buroc';
		$auth->add($updateBuroc);
		
		$deleteBuroc = $auth->createPermission('deleteBuroc');
		$deleteBuroc->description = 'Admin can delete  buroc';
		$auth->add($deleteBuroc);
		
		$indexBuroc = $auth->createPermission('indexBuroc');
		$indexBuroc->description = 'Admin can view all burocs';
		$auth->add($indexBuroc);
		
		$viewBuroc = $auth->createPermission('viewBuroc');
		$viewBuroc->description = 'Admin can view each buroc';
		$auth->add($viewBuroc);
		
		$deleteSummary = $auth->createPermission('deleteSummary');
		$deleteSummary->description = 'Admin can delete  summary';
		$auth->add($deleteSummary);
		
		$deletePoject = $auth->createPermission('deletePoject');
		$deletePoject->description = 'Admin can delete  project';
		$auth->add($deletePoject);
		
		$createSuppliers = $auth->createPermission('createSuppliers');
		$createSuppliers->description = 'Admin can create new supplier';
		$auth->add($createSuppliers);
		
		$updateSuppliers = $auth->createPermission('updateSuppliers');
		$updateSuppliers->description = 'Admin can update  supplier';
		$auth->add($updateSuppliers);
		
		$indexSuppliers = $auth->createPermission('indexSuppliers');
		$indexSuppliers->description = 'Admin can view all suppliers';
		$auth->add($indexSuppliers);
		
		$viewSuppliers = $auth->createPermission('viewSuppliers');
		$viewSuppliers->description = 'Admin can view each supplier';
		$auth->add($viewSuppliers);
		
		$deleteSuppliers = $auth->createPermission('deleteSuppliers');
		$deleteSuppliers->description = 'Admin can delete  supplier';
		$auth->add($deleteSuppliers);
		
		$createInvitations = $auth->createPermission('createInvitations');
		$createInvitations->description = 'Admin can create new invitation';
		$auth->add($createInvitations);
		
		$updateInvitations = $auth->createPermission('updateInvitations');
		$updateInvitations->description = 'Admin can update  invitation';
		$auth->add($updateInvitations);
		
		$indexInvitations = $auth->createPermission('indexInvitations');
		$indexInvitations->description = 'Admin can view all invitations';
		$auth->add($indexInvitations);
		
		$viewInvitations = $auth->createPermission('viewInvitations');
		$viewInvitations->description = 'Admin can view each invitation';
		$auth->add($viewInvitations);
		
		$deleteInvitations = $auth->createPermission('deleteInvitations');
		$deleteInvitations->description = 'Admin can delete  invitation';
		$auth->add($deleteInvitations);
		
		$createRevenues = $auth->createPermission('createRevenues');
		$createRevenues->description = 'Admin can create new revenue';
		$auth->add($createRevenues);
		
		$updateRevenues = $auth->createPermission('updateRevenues');
		$updateRevenues->description = 'Admin can update  revenue';
		$auth->add($updateRevenues);
		
		$indexRevenues = $auth->createPermission('indexRevenues');
		$indexRevenues->description = 'Admin can view all revenues';
		$auth->add($indexRevenues);
		
		$viewRevenues = $auth->createPermission('viewRevenues');
		$viewRevenues->description = 'Admin can view each revenue';
		$auth->add($viewRevenues);
		
		$deleteRevenues = $auth->createPermission('deleteRevenues');
		$deleteRevenues->description = 'Admin can delete  revenue';
		$auth->add($deleteRevenues);
		
		$createSubscribers = $auth->createPermission('createSubscribers');
		$createSubscribers->description = 'Admin can create new subscriber';
		$auth->add($createSubscribers);
		
		$updateSubscribers = $auth->createPermission('updateSubscribers');
		$updateSubscribers->description = 'Admin can update  subscriber';
		$auth->add($updateSubscribers);
		
		$indexSubscribers = $auth->createPermission('indexSubscribers');
		$indexSubscribers->description = 'Admin can view all subscribers';
		$auth->add($indexSubscribers);
		
		$viewSubscribers = $auth->createPermission('viewSubscribers');
		$viewSubscribers->description = 'Admin can view each subscriber';
		$auth->add($viewSubscribers);
		
		$deleteSubscribers = $auth->createPermission('deleteSubscribers');
		$deleteSubscribers->description = 'Admin can delete  subscriber';
		$auth->add($deleteSubscribers);
		
		$createVisitors = $auth->createPermission('createVisitors');
		$createVisitors->description = 'Admin can create new visitor';
		$auth->add($createVisitors);
		
		$updateVisitors = $auth->createPermission('updateVisitors');
		$updateVisitors->description = 'Admin can update  visitor';
		$auth->add($updateVisitors);
		
		$indexVisitors = $auth->createPermission('indexVisitors');
		$indexVisitors->description = 'Admin can view all visitors';
		$auth->add($indexVisitors);
		
		$viewVisitors = $auth->createPermission('viewVisitors');
		$viewVisitors->description = 'Admin can view each visitor';
		$auth->add($viewVisitors);
		
		$deleteVisitors = $auth->createPermission('deleteVisitors');
		$deleteVisitors->description = 'Admin can delete  visitor';
		$auth->add($deleteVisitors);
		
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$employee = $auth->getRole('employee');
		
		$updateOwnUser = $auth->getPermission('updateOwnUser');
		$auth->addChild($employee, $updateOwnUser);
		
		$updateOwnPassword = $auth->getPermission('updateOwnPassword');
		$auth->addChild($employee, $updateOwnPassword);

		$viewOwnUser = $auth->getPermission('viewOwnUser');
		$auth->addChild($employee, $viewOwnUser);
			

		$viewOwnEmployee = $auth->getPermission('viewOwnEmployee');
		$auth->addChild($employee, $viewOwnEmployee);
		
		
		$indexEmployee = $auth->getPermission('indexEmployee');
		$auth->addChild($employee, $indexEmployee);
		
		$viewSummary = $auth->getPermission('viewSummary');
		$auth->addChild($employee, $viewSummary);
		
		
		$indexSummary = $auth->getPermission('indexSummary');
		$auth->addChild($employee, $indexSummary);
		
		$viewProject = $auth->getPermission('viewProject');
		$auth->addChild($employee, $viewProject);
		
		$indexProject = $auth->getPermission('indexProject');
		$auth->addChild($employee, $indexProject);
		
		
		
		
		
		
		
	
		
		
		
		
		
		$teamleader = $auth->getRole('teamleader');
		$auth->addChild($teamleader, $employee);
		
		$createSummary = $auth->getPermission('createSummary');
		$auth->addChild($teamleader, $createSummary);

		$updateSummary = $auth->getPermission('updateSummary');
		$auth->addChild($teamleader, $updateSummary);
		
		
		$createProject = $auth->getPermission('createProject');
		$auth->addChild($teamleader, $createProject);
		
		$updateProject = $auth->getPermission('updateProject');
		$auth->addChild($teamleader, $updateProject);
		
		$updateOwnProject = $auth->getPermission('updateOwnProject');
		$auth->addChild($teamleader, $updateOwnProject);
		
		$updateOwnSummary = $auth->getPermission('updateOwnSummary');
		$auth->addChild($teamleader, $updateOwnSummary);
		
		
		
		
		
		
		
		
		
		
		
		
		
		$admin = $auth->getRole('admin');
		$auth->addChild($admin, $teamleader);		
		
		$createUser = $auth->getPermission('createUser');
		$auth->addChild($admin, $createUser);

		$updateUser = $auth->getPermission('updateUser');
		$auth->addChild($admin, $updateUser);
		
		$deleteUser = $auth->getPermission('deleteUser');
		$auth->addChild($admin, $deleteUser);		

		$updatePassword = $auth->getPermission('updatePassword');
		$auth->addChild($admin, $updatePassword);
		
		$indexUser = $auth->getPermission('indexUser');
		$auth->addChild($admin, $indexUser);
		
		$viewUser = $auth->getPermission('viewUser');
		$auth->addChild($admin, $viewUser);
		
		$createEmployee = $auth->getPermission('createEmployee');
		$auth->addChild($admin, $createEmployee);
		
		$updateEmployee = $auth->getPermission('updateEmployee');
		$auth->addChild($admin, $updateEmployee);
		
		$deleteEmployee = $auth->getPermission('deleteEmployee');
		$auth->addChild($admin, $deleteEmployee);
		
		$viewEmployee = $auth->getPermission('viewEmployee');
		$auth->addChild($admin, $viewEmployee);
		
		$createBuroc = $auth->getPermission('createBuroc');
		$auth->addChild($admin, $createBuroc);
		
		$updateBuroc = $auth->getPermission('updateBuroc');
		$auth->addChild($admin, $updateBuroc);
		
		$deleteBuroc = $auth->getPermission('deleteBuroc');
		$auth->addChild($admin, $deleteBuroc);
		
		$indexBuroc = $auth->getPermission('indexBuroc');
		$auth->addChild($admin, $indexBuroc);
		
		$viewBuroc = $auth->getPermission('viewBuroc');
		$auth->addChild($admin, $viewBuroc);
		
		$deleteSummary = $auth->getPermission('deleteSummary');
		$auth->addChild($admin, $deleteSummary);
		
		$createSuppliers = $auth->getPermission('createSuppliers');
		$auth->addChild($admin, $createSuppliers);
		
		$updateSuppliers = $auth->getPermission('updateSuppliers');
		$auth->addChild($admin, $updateSuppliers);
		
		$indexSuppliers = $auth->getPermission('indexSuppliers');
		$auth->addChild($admin, $indexSuppliers);
		
		$viewSuppliers = $auth->getPermission('viewSuppliers');
		$auth->addChild($admin, $viewSuppliers);
		
		$deleteSuppliers = $auth->getPermission('deleteSuppliers');
		$auth->addChild($admin, $deleteSuppliers);
		
		$createInvitations = $auth->getPermission('createInvitations');
		$auth->addChild($admin, $createInvitations);
		
		$updateInvitations = $auth->getPermission('updateInvitations');
		$auth->addChild($admin, $updateInvitations);
		
		$indexInvitations = $auth->getPermission('indexInvitations');
		$auth->addChild($admin, $indexInvitations);
		
		$viewInvitations = $auth->getPermission('viewInvitations');
		$auth->addChild($admin, $viewInvitations);
		
		$deleteInvitations = $auth->getPermission('deleteInvitations');
		$auth->addChild($admin, $deleteInvitations);
		
		$createRevenues = $auth->getPermission('createRevenues');
		$auth->addChild($admin, $createRevenues);
		
		$updateRevenues = $auth->getPermission('updateRevenues');
		$auth->addChild($admin, $updateRevenues);
		
		$indexRevenues = $auth->getPermission('indexRevenues');
		$auth->addChild($admin, $indexRevenues);
		
		$viewRevenues = $auth->getPermission('viewRevenues');
		$auth->addChild($admin, $viewRevenues);
		
		$deleteRevenues = $auth->getPermission('deleteRevenues');
		$auth->addChild($admin, $deleteRevenues);
		
		$createSubscribers = $auth->getPermission('createSubscribers');
		$auth->addChild($admin, $createSubscribers);
		
		$updateSubscribers = $auth->getPermission('updateSubscribers');
		$auth->addChild($admin, $updateSubscribers);
		
		$indexSubscribers = $auth->getPermission('indexSubscribers');
		$auth->addChild($admin, $indexSubscribers);
		
		$viewSubscribers = $auth->getPermission('viewSubscribers');
		$auth->addChild($admin, $viewSubscribers);
		
		$deleteSubscribers = $auth->getPermission('deleteSubscribers');
		$auth->addChild($admin, $deleteSubscribers);
		
		$createVisitors = $auth->getPermission('createVisitors');
		$auth->addChild($admin, $createVisitors);
		
		$updateVisitors = $auth->getPermission('updateVisitors');
		$auth->addChild($admin, $updateVisitors);
		
		$indexVisitors = $auth->getPermission('indexVisitors');
		$auth->addChild($admin, $indexVisitors);
		
		$viewVisitors = $auth->getPermission('viewVisitors');
		$auth->addChild($admin, $viewVisitors);
		
		$deleteVisitors = $auth->getPermission('deleteVisitors');
		$auth->addChild($admin, $deleteVisitors);
		
		
	}
}