<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head 
style = 
"
 height: 400px;
    background: url('http://liadni.webni.co.il/כוס.jpg') no-repeat center center;
    min-height:100%;
    background-size:100px 150px;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    width: 100%;
    -o-background-size: cover;
position: absolute;
top: 0;
left: 0;
right: 0;

">
    
	
	
	<meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
		 
        'brandLabel' => 'Prat.webni.co.il',
        //'brandUrl' => 'index',
		//'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
			 /* ['label' => 'דף הבית', 'url' => ['/site/index']],
			['label' => 'עובדים', 'url' => ['employees/index']],
			['label' => 'בירוקרטיה', 'url' => ['buroc/index']],
			['label' => 'סיכום יום', 'url' => ['summary-day/index']],
			['label' => 'מי אנחנו', 'url' => ['/site/about']],
            ['label' => 'צור קשר', 'url' => ['/site/contact']],*/
			
			Yii::$app->user->isGuest ?
			['label' => 'דף הבית', 'url' => ['index']]
			:
			 [
            'label' => 'ניהול עובדים ומטלות משרד',
            'items' => [
                // ['label' => 'דף הבית', 'url' => ['site/index']],
                 '<li class="divider"></li>',
                 //'<li class="dropdown-header">תפריט מנהל</li>',
                ['label' => 'עובדים', 'url' => ['employees/index']],
				['label' => 'משרד', 'url' => ['buroc/index']],
				 ['label' => 'פרויקטים', 'url' => ['projects/index']],
				 ['label' => 'משתמשים', 'url' => ['user/index']],
            ],
        ],
		
		
		Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			 [
            'label' => 'נתוני מבקרים',
            'items' => [
                 //['label' => 'דף הבית', 'url' => ['site/index']],
                 '<li class="divider"></li>',
                 //'<li class="dropdown-header">תפריט מנהל</li>',
             ['label' => 'סיכום יומי', 'url' => ['summary-day/index']],
				['label' => 'מבקרים', 'url' => ['visitors/index']],
				 
            ],
        ],
		
		Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			 [
            'label' => 'הכנסות הזמנות וספקים',
            'items' => [
                // ['label' => 'דף הבית', 'url' => ['site/index']],
                 '<li class="divider"></li>',
               //  '<li class="dropdown-header">תפריט מנהל</li>',
            	['label' => 'ספקים', 'url' => ['suppliers/index']],
				['label' => 'הזמנות', 'url' => ['invitations/index']],
			
				 
            ],
        ],
			Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			 [
            'label' => 'ניתוח נתונים - גרפים',
            'items' => [
                // ['label' => 'דף הבית', 'url' => ['site/index']],
                 '<li class="divider"></li>',
              
            		['label' => 'הכנסות', 'url' => ['revenues/index']],
									['label' => 'מנויים', 'url' => ['subscribers/index']],

					   '<li class="dropdown-header">גרפים</li>',
				['label' => 'הכנסות לפי שנים', 'url' => ['summary-day/chart2']],
				['label' => 'מבקרים לפי עונות', 'url' => ['summary-day/chart1']],
				['label' => 'מנויים', 'url' => ['summary-day/chart3']],
				 
            ],
        ],
			
			
			/*Yii::$app->user->isGuest ?
			['label' => 'דף הבית', 'url' => ['index']]
			:
			['label' => 'עובדים', 'url' => ['employees/index']]
			
			,
			
		   Yii::$app->user->isGuest ? 
			['label' => 'מי אנחנו', 'url' => ['/site/about']]
			:
			['label' => 'משרד', 'url' => ['buroc/index']]
			,
        
			Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			['label' => 'פרויקטים', 'url' => ['projects/index']]
			,*/
			
			/*Yii::$app->user->isGuest ?
			['label' => 'צור קשר', 'url' => ['/site/contact']]
			:
			['label' => 'סיכום יומי', 'url' => ['summary-day/index']]
			,
			*/
			
			
			/*Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			['label' => 'משתמשים', 'url' => ['user/index']]
			,
			
			Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			['label' => 'ספקים', 'url' => ['suppliers/index']]
			,
			
			Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			['label' => 'הזמנות', 'url' => ['invitations/index']]
			,
			
			/*Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			['label' => 'פריטים', 'url' => ['item/index']]
			,
		
			Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			['label' => 'הכנסות', 'url' => ['revenues/index']]
			,
			
			/*Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			['label' => 'מבקרים', 'url' => ['visitors/index']]
			,
			
			Yii::$app->user->isGuest ?
			['label' => '', 'url' => ['index']]
			:
			['label' => 'מנויים', 'url' => ['subscribers/index']]
			,
			*/
			
			
			
			
			'',
            Yii::$app->user->isGuest ? (
                ['label' => 'כניסה', 'url' => ['login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'יציאה מהמערכת (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><strong> Webni.co.il  <?= date('Y') ?> &copy; </strong></p>

        <!--<p class="pull-right"><?= Yii::powered() ?></p>-->
		<p class="pull-right"><strong>Powered by Liad Nizri & Eliraz Shimon</strong></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
