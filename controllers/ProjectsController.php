<?php

namespace app\controllers;

use Yii;
use app\models\Projects;
use app\models\ProjectsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
       return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (!\Yii::$app->user->can('indexProject'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionExport()
    {
		$employeeExport= Projects::find()->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $employeeExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['id','define_project','team_leader','employee','due_date','location','notes'], //without header working, because the header will be get label from attribute label. 
			'headers' => ['id' => 'מספר פרויקט', 'define_project' => 'הגדרת המשימה','team_leader' => 'ראש צוות', 'employee' => 'עובדים', 'due_date' => 'תאריך יעד', 'location' => 'מיקום', 'notes' => 'הערות'], 
			'fileName' => 'Projects',
	
		]);
	}

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

		if (!\Yii::$app->user->can('viewProject'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        
				$model = new Projects();
                if(isset($_POST['Projects']))
                {
                        $model->attributes=$_POST['Projects'];
                        if($model->employee!=='')
                                $model->employee=implode(', ',$model->employee);//converting to string...
                        if($model->save())
                                $this->redirect(array('view','id'=>$model->id));
                }
				   else {
            return $this->render('create', [
                'model' => $model,
            ]);
               $model->employee=explode(', ',$model->employee);//converting to array...
                $this->render('create',array(
                        'model'=>$model,
                ));
        }
		}
		/////////////////////////////////////////////////////
		
		
	/*	
		
		$model = new Projects();
=======
		if (!\Yii::$app->user->can('createProject'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $model = new Projects();
>>>>>>> 1dde1c1c5d28e96022c413373e06da8ff69eb25a

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		if (!\Yii::$app->user->can('updateProject'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $model = $this->findModel($id);
$model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())){
        
                        $model->employee = implode(",", $model->employee);
                        
                if($model->save()){
                                return $this->redirect(['view', 'id' => $model->id]);
                        }
            
        } else {
                $model->employee = explode(',',$model->employee);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if (!\Yii::$app->user->can('deletePoject'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
